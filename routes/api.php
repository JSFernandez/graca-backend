<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CreditosController;
use App\Http\Controllers\DocumentosController;
use App\Http\Controllers\EmpleadoController;
use App\Http\Controllers\VacacionesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/','UsuarioController@index');
// Route::name('print')->get('/imprimir', 'GeneradorController@imprimir');

Route::group(['prefix'=>'v1', 'middleware'=>'cors'], function () {
    //Auth
    Route::post('login',[AuthController::class,'login']);
    Route::post('register',[AuthController::class,'register']);
    Route::post('logout',[AuthController::class,'logout']);
    //Roles
    Route::apiResource('role', 'RolController');
    //Permisos
    Route::apiResource('permission', 'PermissionController');
    //Empleado
    Route::apiResource('empleado', 'EmpleadoController');
    Route::get('empleados/combos',[EmpleadoController::class,'combos']);
    Route::post('empleado/search',[EmpleadoController::class,'search']);
    Route::apiResource('usuario', 'UsuarioController');
    Route::post('documentos',[DocumentosController::class,'GeneratePDF']);
    Route::apiResource('credito','CreditosController');
    Route::apiResource('vacaciones','VacacionesController');
    Route::post('vacaciones/search',[VacacionesController::class,'search']);
    //menu
    Route::apiResource('sidebar', 'SidebarController');
});
