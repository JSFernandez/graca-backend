/**
 * { item_description }
 */
$(".formlDinamic").on('submit', function(e) {
    e.preventDefault();
    var id = $(this).attr('id');
    var url = $(this).attr("action");
    var method = $(this).attr("method");
    var forml = $(this);

    if (id == 'guardarRegistro') {
        saveData(url, forml, method);
    }

    if (id == 'guardarRegistroMultitap') {
        var inputFile = $('#file');
        saveDataMultitap(url, forml, method, inputFile);
    }
    if (id == 'DataUpdate') {
        //input file 
        var inputFile = $('#file-2');

        if(inputFile[0] == undefined){
            saveData(url, forml, method);
        }else{
            saveDataMultitap(url, forml, method, inputFile);
        }
    }
    if (id == 'eliminarRegistro'){
        deleteReg(url,forml,method);
    }
});
/**
 * { function_description }
  * @param      {<type>}  url     The url
 */
function deleteReg(url){
    var token = $("#token").attr("content");
    $.ajax({
        url: url,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        success: function(result) {
            location.reload();
        },
        error: function(msj) {
            var message = msj.responseText;
            var errors = $.parseJSON(msj.responseText);
            $.each(errors.errors, function(key, value) {
                toastr.error(value,"Error");
            });
        },
        timeout: 15000
    });
}

/**
 * Saves a data.
 *
 * @param      {<type>}    url     The url
 * @param      {<type>}    forml   The forml
 * @param      {Function}  method  The method
 */
function saveData(url, forml, method) 
{
    var route = $('#route').val();
    $.ajax({
        url: url,
        type: method,
        data: forml.serialize(),
        cache: false,
        success: function(result) 
        {
            $('.modal').modal('hide');// Oculta el modal del formulario create
            // $('#tbody').load(' .tbody');//Recarga el body de la tabla
            // toastr.success(result.message,"Exitoso");
            if(route == null || result.operator == true){
                location.reload();
            }else{
                window.location.replace(route)
            }
        },
        error: function(msj) 
        {
            var status = msj.statusText;
            var errors = $.parseJSON(msj.responseText);
        
            $.each(errors.errors, function(key, value) 
            {
                $("#" + key + "_group").addClass("has-error");
                $("." + key + "_span").addClass("help-block text-danger").html(value);
                toastr.error(value,"Error");
            });
        },
    timeout: 15000
    });
}

/**
 * { function_description }
 * @param      {<type>}    url     The url
 * @param      {<type>}    forml   The forml
 * @param      {Function}  method  The method
 */
function saveDataMultitap(url, forml, method, inputFile) 
{
    var route = $('#route').val();
    var formData = new FormData();
    if (inputFile != null) 
    {
        formData.append('file', inputFile[0].files[0]);
    }
        
    $.ajax({
        url: url + '?' + forml.serialize(),
        type: method,
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
    
        success: function(result) 
        {
            $('.modal').modal('hide');// Oculta el modal del formulario create
            // $('#tbody').load(' .tbody');//Recarga el body de la tabla
            // toastr.success(result.message,"Exitoso");
            if(route == null){
                location.reload();
            }else{
                location(route)
            }
        },
        error: function(msj) 
        {
            var status = msj.statusText;
            var errors = $.parseJSON(msj.responseText);
        
            $.each(errors.errors, function(key, value) 
            {
                $("#" + key + "_group").addClass("has-error");
                $("." + key + "_span").addClass("help-block text-danger").html(value);
                toastr.error(value,"Error");
            });
        },
    timeout: 15000
    });
}

/**
 * { function_description }
 * @param      {string}  url     dirección del controlador para obtener los datos
 * @param      {string}  url2    dirección que se agrega en el acción para guadar los datos
 */
function obtenerDatosGet(url, url2)
{
    var form = $('.form'); //seleciona el formulario
    form.attr('action',url2); 
    $.get(url, function(data)
    {
        $.each(data, function(key, value) 
        {
            console.log(data);
            $('#'+'_'+key).val(value);
        });
        var image = data.image;
        validatFile(image, 'file-2');

    });
}
/**
 * { function_description }
 *
 * @param      {string}  image   The image
 * @param      {string}  id      The identifier
 */
function validatFile(image, id)
{
    if (image != null) 
        {
            var url = './././img/avatar/' + image;
            // destroy fileimput previous
            $('#'+id).fileinput('destroy');
            addImage(url, id, image);
        }
}
/**
 * Adds an image.
 *
 * @param      {<type>}  url       The url
 * @param      {string}  id        The identifier
 * @param      {<type>}  namefile  The namefile
 */
function addImage(url, id, namefile) 
{
    $("#"+id).fileinput
    ({
        initialPreview: [url],
        initialPreviewAsData: true,
        initialPreviewConfig: 
            [
                {caption: namefile},
            ],
        showCaption: false,
        showRemove: false,
        showUpload: false,
        showBrowse: false,
        overwriteInitial: true,
        browseOnZoneClick: true,
        initialCaption: namefile
    });
}
/**
 * Shows the data.
 *
 * @param      {<type>}  url     The url
 */
function showData(url)
{
    $.get(url, function(data)
    {
        $.each(data, function(key, value) 
        {
            console.log(key);
            $('#'+'-'+key).val(value);
        });
        var image = data.image;
        validatFile(image, 'file-3');
    });
}
/**
 * { function_description }
 * @param     {string}  url The url
 */
function modal(url)
{
    var form = $('.form'); //seleciona el formulario
    form.attr('action',url); //agrega url en el action
    form.attr('method', 'POST'); //agrega url en el action
    $("#guardarRegistro")[0].reset();
    $('#myModal').modal('show');
}
/**
 * { function_description }
  * @param      {string}  id      The identifier
 */
function inputClear(id)
{
    $('.'+id+'_span').empty();
    $('#'+id+'_group').removeClass("has-error");
}


function dataTableExport(title, columns) {
    $('#example2').DataTable( {
        lengthChange: true,
        lengthMenu:[10,25,50,100],
        dom: "<'row'<'col-sm-12 col-md-2'l><'col-sm-12 col-md-4'B><'col-sm-12 col-md-6'f>>t<ip>",
        buttons: [
            {
                extend: 'excel',
                title: title,
                text: '<img src="http://localhost/Gaeti/public/img/excel-ico.png" alt="" heigth /> Export',
                titleAttr: 'Excel',
                exportOptions: {
                    columns: columns
                }

            }
        ],
    } );
}