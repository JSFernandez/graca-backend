<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        body {
            font-family:'Times New Roman', Times, serif;
            font-size: 15px;
        }
        @page {
            margin: 2cm 3cm 2cm 3cm;
        }

        footer {
            position: absolute;
            width: 100%;
            bottom: 0px;
            font-size: 11px;
            font-style: italic;
        }
        .firma{
            margin-bottom: -20px;
        }
        .linea-firma{
            width: 250px;
            border-bottom: solid 1px #000;
            margin-left: auto;
            margin-right: auto;

        }

        .text-center {
            text-align: center;
        }

        .text-lef {
            text-align: left;
        }

        .text-right {
            text-align: right;
        }

        .text-justify {
            text-align: justify;
        }

        .container {
            text-align: justify;
            line-height: 30px;
        }

        .sello{
            position: absolute;
            right:0px;
            bottom: 200px;
            transform: rotate(-32)
        }
    </style>
</head>

<body>
    <div class="container">
        <!-- <p>{{$messageData['nombres']}}</p> -->
        <p>
            <img src="{{asset('assets/img/app/logo1.jpg')}}" alt="">
        </p>
        <p class="text-right">
            Caracas, <b>{{date("d")}}</b> de <b>{{date("M")}}</b> de <b>{{date("Y")}}</b>
        </p>
        <p>
            A quien pueda intersar.
        </p>
        <p>
        <h3 class="text-center">
            Constancia de Trabajo
        </h3>
        </p>
        <p>
            Por medio de la presente hacemos constar que <!-- el <b>Ing.</b> --> <b>{{$messageData['apellidos']}} {{$messageData['nombres']}}</b>, titular de la cédula de identidad
            número <b>{{$messageData['persona']['nacionalidad']}} - {{$messageData['persona']['cedula']}}</b>, presta sus servicios actualmente dentro de nuestra organicación desde el
            <b>{{$messageData['fecha_ingreso']}}</b> desempeñando el cargo de <b>{{$messageData['cargo']['valor']}}</b> devengando un salario
            mensual de <b>{{$messageData['monto']}} ({{$messageData['honorarios']}})</b> período durante el cual a cumpledo
            eficazmenete las normas y procedimientos de la empresa.
        </p>
        <br>
        <p class="text-center"><b>Atentamente:</b></p>
        <p class="text-center firma"><img src="{{asset('assets/img/app/firma-zaymelis.jpg')}}" alt=""></p>
        <p class="linea-firma"></p>
        <p class="text-center">Ing Saimelyz Bonalde <br> <b>Departamento RRHH.</b></p>
        <p class="sello"><img src="{{asset('assets/img/app/sello-graca.jpg')}}" alt=""></p>
    </div>
    <footer>
        <p class="text-center">
            Av. Este seis Edif. Centro Parque Carabobo, Torre A, Piso 5, Ofic. 5-09, Urb. La Candelaria, Caracas,
            Venezuela
        </p>
        <p class="text-center">
            Teléfonos: (0212) 577-04-43
        </p>
    </footer>
</body>

</html>
