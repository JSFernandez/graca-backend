<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirecciones extends Migration
{
    private $name = 'direcciones';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->name, function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->unsignedBigInteger('estado')->index();
            $table->unsignedBigInteger('municipio')->index();
            $table->unsignedBigInteger('parroquia')->index();
            $table->foreign('estado')->references('id')->on('nomencladores')->onDelete('cascade');
            $table->foreign('municipio')->references('id')->on('nomencladores')->onDelete('cascade');
            $table->foreign('parroquia')->references('id')->on('nomencladores')->onDelete('cascade');
            $table->text('detalles');
            $table->string('codigo_postal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->name);
    }
}
