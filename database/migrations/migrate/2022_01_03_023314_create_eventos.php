<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventos extends Migration
{
    private $name = 'eventos';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->name, function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->time('hota_inicio');
            $table->time('hota_fin');
            $table->string('descripcion');
            $table->unsignedBigInteger('tipo')->index();
            $table->foreign('tipo')->references('id')->on('nomencladores')->onDelete('cascade');
            $table->string('imagen');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->name);
    }
}
