<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuentas extends Migration
{
    private $name = 'cuentas';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->name, function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->unsignedBigInteger('banco')->index();
            $table->unsignedBigInteger('tipo')->index();
            $table->foreign('banco')->references('id')->on('nomencladores')->onDelete('cascade');
            $table->foreign('tipo')->references('id')->on('nomencladores')->onDelete('cascade');
            $table->string('numero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->name);
    }
}
