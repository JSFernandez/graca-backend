<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->unsignedBigInteger('persona')->index();
            $table->string('ct');
            $table->unsignedBigInteger('cargo')->index();
            $table->foreign('persona')->references('id')->on('personas')->onDelete('cascade');
            $table->foreign('cargo')->references('id')->on('nomencladores')->onDelete('cascade');
            $table->date('fecha_ingreso');
            $table->integer('honorarios');
            $table->unsignedBigInteger('correo')->index();
            $table->unsignedBigInteger('contacto')->index();
            $table->foreign('correo')->references('id')->on('correos')->onDelete('cascade');
            $table->foreign('direccion')->references('id')->on('direccion')->onDelete('cascade');
            $table->foreign('contacto')->references('id')->on('personas')->onDelete('cascade');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
