<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('cedula');
            $table->unsignedBigInteger('nacionalidad')->index();
            $table->foreign('nacionalidad')->references('id')->on('nomencladores');
            $table->string('p_nombre');
            $table->string('p_apellido');
            $table->string('s_nombre');
            $table->string('s_apellido');
            $table->date('fecha_nacimiento');
            $table->string('sexo');
            $table->string('foto');
            $table->string('status');
            $table->unsignedBigInteger('talla_superior')->index();
            $table->unsignedBigInteger('talla_inferior')->index();
            $table->unsignedBigInteger('talla_calzado')->index();
            $table->unsignedBigInteger('grupo_sanguineo')->index(); 
            $table->unsignedBigInteger('telefono_movil')->index();
            $table->unsignedBigInteger('telefono_local')->index();
            $table->foreign('talla_superior')->references('id')->on('nomencladores')->onDelete('cascade');
            $table->foreign('talla_inferior')->references('id')->on('nomencladores')->onDelete('cascade');
            $table->foreign('talla_calzado')->references('id')->on('nomencladores')->onDelete('cascade');
            $table->foreign('grupo_sanguineo')->references('id')->on('nomencladores')->onDelete('cascade');
            $table->foreign('telefono_movil')->references('id')->on('telefonos')->onDelete('cascade');
            $table->foreign('telefono_local')->references('id')->on('telefonos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
