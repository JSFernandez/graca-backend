-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 28, 2023 at 10:08 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `graca`
--

-- --------------------------------------------------------

--
-- Table structure for table `correos`
--

CREATE TABLE `correos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `tipo` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `correos`
--

INSERT INTO `correos` (`id`, `direccion`, `tipo`, `status`) VALUES
(1, 'correo@email.com', 1, 0),
(2, 'sherman589@gmail.com', 1, 1),
(3, 'prueba1@gmail.com', 1, 1),
(4, 'prueba1@gmail.com', 1, 1),
(5, 'prueba1@gmail.com', 1, 1),
(6, 'prueba@gmail.com', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `creditos`
--

CREATE TABLE `creditos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `monto` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `cuotas` varchar(4) NOT NULL,
  `moneda` varchar(2) NOT NULL,
  `monto_descuento` varchar(255) NOT NULL,
  `frecuencia_descuento` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `creditos`
--

INSERT INTO `creditos` (`id`, `monto`, `fecha_inicio`, `fecha_fin`, `cuotas`, `moneda`, `monto_descuento`, `frecuencia_descuento`, `status`, `created_at`, `updated_at`) VALUES
(4, 2000, '2023-07-19', '2023-07-31', '12', '$', '200', 'quincenal', 'aprobado', '2023-07-19 23:45:47', '2023-07-19 23:45:47'),
(5, 500, '2023-07-19', '2023-07-31', '5', '$', '100', 'quincenal', 'aprobado', '2023-07-19 23:59:35', '2023-07-19 23:59:35');

-- --------------------------------------------------------

--
-- Table structure for table `creditos_empleados`
--

CREATE TABLE `creditos_empleados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `credito_id` bigint(20) UNSIGNED NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `creditos_empleados`
--

INSERT INTO `creditos_empleados` (`id`, `credito_id`, `empleado_id`, `created_at`, `updated_at`) VALUES
(2, 4, 19, '2023-07-19 23:45:47', '2023-07-19 23:45:47'),
(3, 5, 19, '2023-07-19 23:59:35', '2023-07-19 23:59:35');

-- --------------------------------------------------------

--
-- Table structure for table `cuentas`
--

CREATE TABLE `cuentas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `banco` bigint(20) UNSIGNED NOT NULL,
  `tipo` bigint(20) UNSIGNED NOT NULL,
  `numero` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `direcciones`
--

CREATE TABLE `direcciones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `estado` bigint(20) UNSIGNED NOT NULL,
  `municipio` bigint(20) UNSIGNED NOT NULL,
  `parroquia` bigint(20) UNSIGNED NOT NULL,
  `detalles` text NOT NULL,
  `codigo_postal` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `direcciones`
--

INSERT INTO `direcciones` (`id`, `estado`, `municipio`, `parroquia`, `detalles`, `codigo_postal`) VALUES
(1, 130, 155, 1, 'Desconocido', '000');

-- --------------------------------------------------------

--
-- Table structure for table `documentos`
--

CREATE TABLE `documentos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `direccion` text NOT NULL,
  `tipo` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `empleados`
--

CREATE TABLE `empleados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `persona` bigint(20) UNSIGNED NOT NULL,
  `ct` varchar(15) NOT NULL,
  `cargo` bigint(20) UNSIGNED NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `honorarios` int(11) NOT NULL,
  `correo` bigint(20) UNSIGNED NOT NULL,
  `contacto` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `empleados`
--

INSERT INTO `empleados` (`id`, `persona`, `ct`, `cargo`, `fecha_ingreso`, `honorarios`, `correo`, `contacto`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, '00001', 79, '2018-05-21', 1000, 1, 1, 'activo', '0000-00-00 00:00:00', '2023-02-23 00:54:21'),
(2, 3, '00000', 1, '2018-09-01', 0, 1, 1, 'vacasiones', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 4, '00000', 79, '2018-03-15', 0, 1, 1, 'inactivo', '0000-00-00 00:00:00', '2023-02-23 04:59:24'),
(4, 5, '00000', 1, '2018-10-10', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 6, '00000', 1, '2018-09-21', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 7, '00000', 1, '2019-01-24', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 8, '00000', 1, '2019-08-19', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 9, '00000', 1, '2019-08-26', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 10, '00000', 1, '2019-09-16', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 11, '00000', 1, '2019-09-23', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 12, '00000', 1, '2019-09-23', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 13, '00000', 1, '2019-10-15', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 14, '00000', 1, '2019-10-15', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 15, '00000', 1, '2019-10-21', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 16, '00000', 1, '2019-10-21', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 17, '00000', 1, '2019-10-21', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 18, '00000', 1, '2019-11-18', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 19, '00000', 1, '2019-12-02', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 20, '24454', 1, '2019-12-16', 1000, 2, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 21, '00000', 1, '2020-06-03', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 22, '00000', 1, '2020-06-16', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 23, '00000', 1, '2020-07-27', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 24, '00000', 1, '2020-10-01', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 25, '00000', 1, '2020-10-01', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 26, '00000', 1, '2020-11-16', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 27, '00000', 1, '2021-02-17', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 28, '00000', 1, '2021-04-05', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 29, '00000', 1, '2021-05-03', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 30, '00000', 1, '2021-05-03', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 31, '00000', 1, '2021-05-04', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 32, '00000', 1, '2021-05-10', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 33, '00000', 1, '2021-05-17', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 34, '00000', 1, '2021-07-01', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 35, '00000', 1, '2021-07-01', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 36, '00000', 1, '2021-07-01', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 37, '00000', 1, '2021-07-12', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 38, '00000', 1, '2021-07-15', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 39, '00000', 1, '2016-10-03', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 40, '00000', 1, '2019-06-15', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 41, '00000', 1, '2021-01-01', 0, 1, 1, 'activo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 49, '00006', 79, '2023-02-22', 1000, 6, 1, '1', '2023-02-23 04:10:28', '2023-02-23 04:10:28');

-- --------------------------------------------------------

--
-- Table structure for table `empleados_conocimientos`
--

CREATE TABLE `empleados_conocimientos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `conocimiento_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `empleados_cuentas`
--

CREATE TABLE `empleados_cuentas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `cuenta_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `empleados_documentos`
--

CREATE TABLE `empleados_documentos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `documento_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `evaluaciones`
--

CREATE TABLE `evaluaciones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `puntos` int(11) NOT NULL,
  `valor` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `observacion` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `evaluaciones_empleados`
--

CREATE TABLE `evaluaciones_empleados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `evaluacion_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `eventos`
--

CREATE TABLE `eventos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `hota_inicio` time NOT NULL,
  `hota_fin` time NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `tipo` bigint(20) UNSIGNED NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `eventos_empleados`
--

CREATE TABLE `eventos_empleados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `evento_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_01_20_084450_create_roles_table', 1),
(4, '2015_01_20_084525_create_role_user_table', 1),
(5, '2015_01_24_080208_create_permissions_table', 1),
(6, '2015_01_24_080433_create_permission_role_table', 1),
(7, '2015_12_04_003040_add_special_role_column', 1),
(8, '2017_10_17_170735_create_permission_user_table', 1),
(9, '2019_08_19_000000_create_failed_jobs_table', 1),
(10, '2022_01_03_014632_create_nomencladores', 1),
(11, '2022_01_03_020127_create_documentos', 1),
(12, '2022_01_03_021527_create_direcciones', 1),
(13, '2022_01_03_022257_create_cuentas', 1),
(14, '2022_01_03_023143_create_correos', 1),
(15, '2022_01_03_023314_create_eventos', 1),
(16, '2022_01_03_024526_create_evaluaciones', 1),
(17, '2022_01_03_025149_create_telefonos', 1),
(18, '2022_01_03_025519_create_personas', 1),
(19, '2022_01_03_030713_create_empleados', 1),
(20, '2022_01_03_121347_create_creditos', 1),
(21, '2022_01_03_232926_create_personas_enfermedades', 1),
(22, '2022_01_03_233157_create_empleados_conocimientos', 1),
(23, '2022_01_03_233232_create_evaluaciones_empleados', 1),
(24, '2022_01_03_234400_create_eventos_empleados', 1),
(25, '2022_01_03_234625_create_empleados_cuentas', 1),
(26, '2022_01_03_235120_create_empleados_documentos', 1),
(27, '2022_01_03_235206_create_creditos_empleados', 1),
(28, '2014_09_12_000001_create_personal_access_tokens_table', 2),
(29, '2019_12_14_000001_create_personal_access_tokens_table', 3),
(30, '2023_05_07_023701_create_permission_tables', 4),
(31, '2023_05_28_195106_create_sidebars_table', 5),
(32, '2023_06_29_152737_vacaciones', 6),
(33, '2023_06_29_152938_vacaciones_empleados', 6);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'AppModelUser', 1),
(2, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2),
(2, 'App\\Models\\User', 8);

-- --------------------------------------------------------

--
-- Table structure for table `nomencladores`
--

CREATE TABLE `nomencladores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `valor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `codigo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `nomencladores`
--

INSERT INTO `nomencladores` (`id`, `valor`, `tipo`, `codigo`, `status`, `created_at`, `updated_at`) VALUES
(1, 'INDEFINIDO', '1', '-1', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'NOMENCLADOR', '1', '1', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'ESTADO VENEZOLANO', '1', '106', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'MUNICIPIO', '1', '107', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'PARROQUIA', '1', '108', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Ciudades', '1', '110', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'TIPO DIRECCI?N', '1', '114', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'ESTADO LABORAL', '1', '131', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'OFICIO', '1', '224', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'NACIONALIDADES', '1', '226', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'CARRERA', '1', '232', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'ESPECIALIDAD', '1', '233', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'PROFESI?N', '1', '234', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'ESTADO CIVIL', '1', '235', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'GRADO INSTRUCCI?N', '1', '236', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'SEXO', '1', '482', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'GRUPO SANGUINEO', '1', '90', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'CARGOS', '1', '91', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'BANCOS', '1', '92', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'CUENTAS BANCARIAS', '1', '93', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'DOCUMENTOS', '1', '94', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'CORREOS', '1', '95', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'TALLA_SUPERIOR', '1', '96', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'TALLA_INFERIOR', '1', '97', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'TALLA_CALZADO', '1', '98', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'SP', '96', '96001', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'SS', '96', '96002', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'SM', '96', '96003', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'MP', '96', '96004', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'MM', '96', '96005', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'LP', '96', '96006', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'LM', '96', '96007', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'LL', '96', '96008', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'XL', '96', '96009', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'XXL', '96', '96010', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'XXXL', '96', '96011', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, '28', '97', '97001', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, '29', '97', '97002', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, '30', '97', '97003', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, '31', '97', '97004', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '32', '97', '97005', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '33', '97', '97006', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, '34', '97', '97007', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, '35', '97', '97008', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, '36', '97', '97009', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, '37', '97', '97010', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, '38', '97', '97011', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, '39', '97', '97012', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, '40', '97', '97013', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, '25', '98', '98001', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, '26', '98', '98002', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, '27', '98', '98003', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, '28', '98', '98004', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, '29', '98', '98005', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, '30', '98', '98006', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, '31', '98', '98007', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, '32', '98', '98008', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, '33', '98', '98009', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, '34', '98', '98010', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, '35', '98', '98011', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, '36', '98', '98012', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, '37', '98', '98013', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, '38', '98', '98014', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, '39', '98', '98015', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, '40', '98', '98016', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, '41', '98', '98017', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, '42', '98', '98018', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, '43', '98', '98019', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, '44', '98', '98020', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, '45', '98', '98021', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 'O+', '90', '90000', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 'O-', '90', '90001', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 'A-', '90', '90002', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 'A+', '90', '90003', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 'B-', '90', '90004', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 'B+', '90', '90005', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 'AB-', '90', '90006', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 'AB+', '90', '90007', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 'Programador', '91', '91000', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 'Arquitecto de Software', '91', '91001', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 'QA', '91', '91002', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 'Desarrollador de BD', '91', '91003', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 'Analista de BD', '91', '91004', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 'Dise?ador Gr?fico', '91', '91005', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 'Adsistente Administrativo', '91', '91006', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 'Gerente', '91', '91007', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 'Obrero', '91', '91008', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 'Asesor Jur?dico', '91', '91009', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 'Full Stack', '91', '91010', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 'Consultor Inform?tico Senior', '91', '91011', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 'Banco de Venezuela', '92', '92000', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 'Banesco', '92', '92001', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, '100% Banco', '92', '92002', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 'Banca Amiga', '92', '92003', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 'Banco Activo', '92', '92004', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 'Banco Agr?cola de Venezuela', '92', '92005', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 'Banco Bicentenario', '92', '92006', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 'Banco de Exportaci?n y Comercio', '92', '92008', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 'Banco del Tesoro', '92', '92009', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 'Banco Exterior', '92', '92010', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 'Banco internacional de Desarrollo', '92', '92011', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 'Banco Mercantil', '92', '92012', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 'Banco Nacional de Cr?dito BNC', '92', '92013', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 'Banco Plaza', '92', '92014', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 'Banco Sofitasa', '92', '92015', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 'Banco Venezolano de Cr?dito', '92', '92016', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 'Bancrecer', '92', '92017', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 'BANFANB', '92', '92018', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 'Bangente', '92', '92019', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(110, 'Banplus', '92', '92020', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(111, 'BBVA Provincial', '92', '92021', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(112, 'BFC Banco Fondo Com?n', '92', '92022', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(113, 'BOD', '92', '92023', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(114, 'DELSUR', '92', '92024', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(115, 'Banesco P', '92', '92025', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(116, 'Mercantil P', '92', '92026', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(117, 'Ahorro', '93', '93000', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(118, 'Corriente', '93', '93001', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(119, 'Electr?nica', '93', '93002', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(120, 'Divisas UDS', '93', '93003', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(121, 'divisas EUR', '93', '93004', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(122, 'Curriculo', '94', '94000', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(123, 'RIF', '94', '94001', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(124, 'Solvencia ISR', '94', '94002', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(125, 'Factura de Pago', '94', '94003', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(126, 'Constancia de Trabajo', '94', '94004', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(127, 'C?dula de Identidad', '94', '94005', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(128, 'Laboral', '95', '95000', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(129, 'Personal', '95', '95000', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(130, 'Distrito Capital', '107', '1300000', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(131, 'Estado Amazonas', '107', '1310000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(132, 'Estado Anzoategui', '107', '1320000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(133, 'Estado Apure', '107', '1330000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(134, 'Estado Aragua', '107', '1340000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(135, 'Estado Barinas', '107', '1350000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(136, 'Estado Bolivar', '107', '1360000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(137, 'Estado Carabobo', '107', '1370000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(138, 'Estado Cojedes', '107', '1380000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(139, 'Estado Delta Amacuro', '107', '1390000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(140, 'Estado Falcon', '107', '1400000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(141, 'Estado Guarico', '107', '1410000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(142, 'Estado Lara', '107', '1420000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(143, 'Estado Merida', '107', '1430000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(144, 'Estado Miranda', '107', '1440000', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(145, 'Estado Monagas', '107', '1450000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(146, 'Estado Nueva Esparta', '107', '1460000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(147, 'Estado Portuguesa', '107', '1470000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(148, 'Estado Sucre', '107', '1480000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(149, 'Estado Tachira', '107', '1490000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 'Estado Trujillo', '107', '1500000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 'Estado Yaracuy', '107', '1510000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 'Estado Zulia', '107', '1520000', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 'Estado La Guaira', '107', '1530000', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(154, 'Dependencias Federales', '107', '1060223', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 'Municipio Libertador', '108', '130', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 'Municipio Alto Orinoco (La Esmeralda)', '108', '131', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 'Municipio Atabapo (San Fernando de Atabapo)', '108', '131', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 'Municipio Atures (Puerto Ayacucho)', '108', '131', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `guard_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'read', 'api', '2023-05-08 03:39:40', '2023-05-08 03:39:40'),
(2, 'write', 'api', '2023-05-08 03:39:50', '2023-05-08 03:39:50'),
(3, 'update', 'api', '2023-05-08 03:39:59', '2023-05-08 03:39:59'),
(4, 'show', 'api', '2023-05-08 03:40:06', '2023-05-08 03:40:06'),
(5, 'create', 'api', '2023-05-08 03:40:14', '2023-05-08 03:40:14'),
(6, 'delete', 'api', '2023-05-08 03:40:20', '2023-05-08 03:40:20'),
(7, 'all', 'api', '2023-05-08 03:42:20', '2023-05-08 03:42:20');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens_2`
--

CREATE TABLE `personal_access_tokens_2` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens_2`
--

INSERT INTO `personal_access_tokens_2` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'appToken', '1274bfcbc9fe3d72bce92622116970ab63981b4cf19dafe8546b571361a40100', '[\"*\"]', NULL, '2022-05-09 23:38:29', '2022-05-09 23:38:29'),
(3, 'App\\Models\\User', 2, 'appToken', '759304a12cdb2de445554162c73387dc4fa7356544292dd88472930bddc715c4', '[\"*\"]', NULL, '2022-05-10 19:57:46', '2022-05-10 19:57:46'),
(4, 'App\\Models\\User', 3, 'appToken', '83f26f04aaea94d639cb74c6174b5affa715c70ed972400cbe214005e6503796', '[\"*\"]', NULL, '2022-05-10 21:37:40', '2022-05-10 21:37:40'),
(5, 'App\\Models\\User', 4, 'appToken', 'd1d947701d91a1d447c9085b306b639e104e45b504b85f96c2241ecfdf379023', '[\"*\"]', NULL, '2022-05-10 21:40:24', '2022-05-10 21:40:24'),
(6, 'App\\Models\\User', 5, 'appToken', 'c5ce97ca63f05c2156040b8b8d3322dbcc961305bd2e0bf3b3e174ea5cc83dd5', '[\"*\"]', NULL, '2022-05-10 21:44:42', '2022-05-10 21:44:42'),
(7, 'App\\Models\\User', 6, 'appToken', '6485073d2e68ec713db28ebf232e3c800afd488961d3f3286c47ff85d0a36552', '[\"*\"]', NULL, '2022-05-10 21:47:02', '2022-05-10 21:47:02'),
(8, 'App\\Models\\User', 7, 'appToken', 'b5c312d34e4961511e3f46622be71bfcd75b6531a8638e0ac9df8c503c094389', '[\"*\"]', NULL, '2022-05-10 21:47:57', '2022-05-10 21:47:57');

-- --------------------------------------------------------

--
-- Table structure for table `personas`
--

CREATE TABLE `personas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cedula` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nacionalidad` bigint(20) UNSIGNED NOT NULL,
  `p_nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_apellido` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_apellido` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `sexo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `talla_superior` bigint(20) UNSIGNED DEFAULT NULL,
  `talla_inferior` bigint(20) UNSIGNED DEFAULT NULL,
  `talla_calzado` bigint(20) UNSIGNED DEFAULT NULL,
  `grupo_sanguineo` bigint(20) UNSIGNED DEFAULT NULL,
  `telefono_movil` bigint(20) UNSIGNED NOT NULL,
  `telefono_local` bigint(20) UNSIGNED DEFAULT NULL,
  `direccion_habitacion` bigint(20) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Dumping data for table `personas`
--

INSERT INTO `personas` (`id`, `cedula`, `nacionalidad`, `p_nombre`, `p_apellido`, `s_nombre`, `s_apellido`, `fecha_nacimiento`, `sexo`, `foto`, `status`, `talla_superior`, `talla_inferior`, `talla_calzado`, `grupo_sanguineo`, `telefono_movil`, `telefono_local`, `direccion_habitacion`, `created_at`, `updated_at`) VALUES
(1, '0', 2271, 'indefinidos', 'indefinidos', 'indefinido', 'indefinido', '1900-01-01', 'I', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '2023-02-23 00:53:29'),
(2, '8.763.264', 2271, 'Maribel', 'Marin', NULL, NULL, '1968-08-31', 'F', '', '1', 26, 37, 50, 1, 1, 1, 1, '0000-00-00 00:00:00', '2023-02-21 20:37:17'),
(3, '3.519.608', 2271, 'Rafael', 'Mezzana', NULL, NULL, '1950-06-01', 'M', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '2023-02-23 04:55:53'),
(4, '6.472.863', 2271, 'Simón', 'Sánchez', NULL, NULL, '1962-11-23', 'M', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '2023-02-23 04:59:24'),
(5, '18.487.832', 2271, 'Héctor', 'Palomino', NULL, NULL, '1989-04-28', 'M', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '2023-02-23 04:59:47'),
(6, '24.463.392', 2271, 'Ricardo ', 'Baptista', '', '', '1993-09-12', 'M', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '21.536.846', 2271, 'Jeremy ', 'Sangroni', '', '', '2019-05-24', 'M', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '25.216.467', 2271, 'Anderson ', 'Gómez', '', '', '1995-09-14', 'M', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, '8.763.264', 2271, 'Juan ', 'Estrada', '', '', '1992-07-07', 'M', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, '3.519.608', 2271, 'Jhonnatan ', 'González', '', '', '1996-01-13', 'M', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, '6.472.863', 2271, 'José ', 'Arias', 'Manuel ', '', '1993-03-19', 'M', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, '18.487.832', 2271, 'Gustavo ', 'Acosta', '', '', '1993-02-05', 'M', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, '24.463.392', 2271, 'Andrés ', 'Leotur', '', '', '1984-10-26', 'M', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, '21.536.846', 2271, 'Katiria ', 'Pérez', '', '', '1982-01-01', 'F', '', '1', 1, 1, 1, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, '25.216.467', 2271, 'Garry ', 'Bruno', '', '', '1995-12-14', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, '20.978.294', 2271, 'Gino ', 'López', 'Martín ', '', '1993-10-31', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, '24.905.659', 2271, 'Freddy ', 'Muñoz', '', '', '1977-10-26', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, '23.527.946', 2271, 'Vanessa ', 'Molina', '', '', '1994-11-12', 'F', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, '20.654.101', 2271, 'Víctor ', 'Marrero', '', '', '1966-11-27', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, '16085382', 2271, 'Jesús ', 'Fernández', '', '', '1982-11-04', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, '15.573.515', 2271, 'Luis ', 'Chirino', '', '', '1989-09-19', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, '24.939.724', 2271, 'Rodolfo ', 'López', '', '', '1967-10-01', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, '28.420.486', 2271, 'Víctor ', 'González', 'Emílio', '', '1995-10-26', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, '13.641.624', 2271, 'Julio ', 'García', '', '', '1963-01-25', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, '18.707.864', 2271, 'Humberto ', 'Goitte', '', '', '1989-11-25', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, '26.738.100', 2271, 'Yesleidy ', 'Becerra', '', '', '1995-10-23', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, '25.215.534', 2271, 'Victor ', 'Gonzalez ', 'Julio ', '', '1999-01-17', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, '23.000.841', 2271, 'Albert  ', 'Torres ', '', '', '1994-12-30', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, '19.649.165', 2271, 'Yhennay ', 'Barreto', '', '', '1989-07-24', 'F', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, '24.940.615', 2271, 'Nolan ', 'Vásquez', '', '', '1996-04-06', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, '22.016.139', 2271, 'Alexander ', 'Ferreira', '', '', '1995-01-09', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, '27.187.129', 2271, 'Roberto ', 'Rivas', '', '', '2020-05-28', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, '20.820.349', 2271, 'Jesús ', 'Rojas', '', '', '1992-06-12', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, '27.223.322', 2271, 'Efrain ', 'Sánchez', '', '', '1999-01-17', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, '26.472.702', 2271, 'Samuel ', 'Díaz', '', '', '1998-04-21', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '21.718.395', 2271, 'Fernando ', 'Obregón', '', '', '1993-06-26', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, '28.463.068', 2271, 'Franklin ', 'Durán', '', '', '2002-05-17', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, '28.180.664', 2271, 'Felipe ', 'Hernández', '', '', '2000-04-20', 'M', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, '15.980.765', 2271, 'Zaymelis ', 'Bonalde', '', '', '1983-07-20', 'F', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, '20.488.786', 2271, 'Yulexi ', 'Yrigoyen', '', '', '1992-08-24', 'F', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, '17.650.565', 2271, 'Hernis ', 'de Flaviis ', '', '', '1986-06-21', 'F', '', '1', 1, 1, 1, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, '16085382', 1, 'Jesus', 'Fernadnez', NULL, NULL, '2022-05-03', 'M', NULL, '1', NULL, NULL, NULL, NULL, 1, 1, 0, '2022-05-03 00:35:37', '2022-05-03 00:35:37'),
(43, '100000', 1, 'Prueba', 'prueba', NULL, NULL, '2023-02-22', 'M', NULL, '1', 26, 37, 50, NULL, 3, 4, 1, '2023-02-23 03:28:22', '2023-02-23 03:28:22'),
(44, '200000', 1, 'Prueba', 'prueba', NULL, NULL, '2023-02-22', 'M', NULL, '1', 26, 37, 50, NULL, 6, 7, 1, '2023-02-23 03:34:03', '2023-02-23 03:34:03'),
(45, '300000', 1, 'Prueba', 'prueba', NULL, NULL, '2023-02-22', 'M', NULL, '1', 26, 37, 50, NULL, 9, 10, 1, '2023-02-23 03:35:27', '2023-02-23 03:35:27'),
(46, '30000', 1, 'prueba', 'prueba', NULL, NULL, '2023-02-22', 'M', NULL, '1', 26, 37, 50, NULL, 12, 13, 1, '2023-02-23 03:48:06', '2023-02-23 03:48:06'),
(47, '40000', 1, 'prueba', 'prueba', NULL, NULL, '2023-02-22', 'M', NULL, '1', 26, 37, 50, NULL, 15, 16, 1, '2023-02-23 03:53:16', '2023-02-23 03:53:16'),
(48, '50000', 1, 'prueba', 'prueba', NULL, NULL, '2023-02-22', 'M', NULL, '1', 26, 37, 50, NULL, 18, 19, 1, '2023-02-23 03:54:02', '2023-02-23 03:54:02'),
(49, '600000', 1, 'prueba', 'prueba', NULL, NULL, '2023-02-22', 'M', NULL, '1', 26, 37, 50, NULL, 21, 22, 1, '2023-02-23 04:10:28', '2023-02-23 04:10:28');

-- --------------------------------------------------------

--
-- Table structure for table `personas_enfermedades`
--

CREATE TABLE `personas_enfermedades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `persona_id` bigint(20) UNSIGNED NOT NULL,
  `enfermedad_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `guard_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', 'api', '2023-05-08 03:37:53', '2023-05-08 03:37:53'),
(2, 'admin', 'api', '2023-05-08 03:38:05', '2023-05-08 03:38:05'),
(3, 'empleado', 'api', '2023-05-08 03:38:11', '2023-05-08 03:38:11'),
(4, 'supervisor', 'api', '2023-05-08 03:38:18', '2023-05-08 03:38:18');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sidebars`
--

CREATE TABLE `sidebars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `path` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sidebars`
--

INSERT INTO `sidebars` (`id`, `path`, `title`, `icon`, `class`, `created_at`, `updated_at`) VALUES
(1, '/dashboard', 'Panel principal', 'dashboard', '', '2023-06-16 21:52:56', '2023-06-16 21:52:56'),
(2, '/user-profile', 'Mi perfil', 'badge', '', '2023-06-16 21:52:56', '2023-06-16 21:52:56'),
(3, '/role-permission', 'Roles-Permisos', 'enhanced_encryption', '', '2023-06-16 21:52:56', '2023-06-16 21:52:56'),
(4, '/usuarios', 'Usuarios', 'people', '', '2023-06-16 21:52:56', '2023-06-16 21:52:56'),
(5, '/empleados', 'Empleados', 'groups', '', '2023-06-16 21:52:56', '2023-06-16 21:52:56'),
(6, '/documentos', 'Documentos', 'picture_as_pdf', '', '2023-06-16 21:52:56', '2023-06-16 21:52:56'),
(7, '/geolocalizacion', 'Geolocalización', 'groups', '', '2023-06-16 21:52:56', '2023-06-16 21:52:56'),
(8, '/calendario', 'Calendario', 'event', '', '2023-06-16 21:52:56', '2023-06-16 21:52:56');

-- --------------------------------------------------------

--
-- Table structure for table `telefonos`
--

CREATE TABLE `telefonos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `numero` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Dumping data for table `telefonos`
--

INSERT INTO `telefonos` (`id`, `numero`, `tipo`, `status`, `created_at`, `updated_at`) VALUES
(1, '000-000-00-00', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '000-000-00-00', 1, 1, '2023-02-23 03:28:21', '2023-02-23 03:28:21'),
(4, '000-00-00-00', 1, 1, '2023-02-23 03:28:22', '2023-02-23 03:28:22'),
(5, '000-000-00-00', 1, 1, '2023-02-23 03:28:22', '2023-02-23 03:28:22'),
(6, '000-000-00-00', 1, 1, '2023-02-23 03:34:03', '2023-02-23 03:34:03'),
(7, '000-00-00-00', 1, 1, '2023-02-23 03:34:03', '2023-02-23 03:34:03'),
(8, '000-000-00-00', 1, 1, '2023-02-23 03:34:03', '2023-02-23 03:34:03'),
(9, '000-000-00-00', 1, 1, '2023-02-23 03:35:26', '2023-02-23 03:35:26'),
(10, '000-00-00-00', 1, 1, '2023-02-23 03:35:27', '2023-02-23 03:35:27'),
(11, '000-000-00-00', 1, 1, '2023-02-23 03:35:27', '2023-02-23 03:35:27'),
(12, '000-000-00-00', 1, 1, '2023-02-23 03:48:06', '2023-02-23 03:48:06'),
(13, '000-000-00-00', 1, 1, '2023-02-23 03:48:06', '2023-02-23 03:48:06'),
(14, '000-000-00-00', 1, 1, '2023-02-23 03:48:06', '2023-02-23 03:48:06'),
(15, '000-000-00-00', 1, 1, '2023-02-23 03:53:16', '2023-02-23 03:53:16'),
(16, '000-000-00-00', 1, 1, '2023-02-23 03:53:16', '2023-02-23 03:53:16'),
(17, '000-000-00-00', 1, 1, '2023-02-23 03:53:16', '2023-02-23 03:53:16'),
(18, '000-000-00-00', 1, 1, '2023-02-23 03:54:02', '2023-02-23 03:54:02'),
(19, '000-000-00-00', 1, 1, '2023-02-23 03:54:02', '2023-02-23 03:54:02'),
(20, '000-000-00-00', 1, 1, '2023-02-23 03:54:02', '2023-02-23 03:54:02'),
(21, '000-000-00-00', 1, 1, '2023-02-23 04:10:28', '2023-02-23 04:10:28'),
(22, '000-000-00-00', 1, 1, '2023-02-23 04:10:28', '2023-02-23 04:10:28'),
(23, '000-000-00-00', 1, 1, '2023-02-23 04:10:28', '2023-02-23 04:10:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(2) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'prueba1', 'prueba1@gmail.com', NULL, '$2y$10$D9CH/0jYiWyPbkL8a44y2un23e1F.dBQtZgPKptmbR11KS23DsQy.', '1', NULL, '2022-05-09 23:38:28', '2023-07-21 17:09:48'),
(2, 'prueba2', 'prueba2@gmail.com', NULL, '$2y$10$TIwXn3wDtuZVxXvXkkwCDO3N/wwa9P5t4GVwPsc4h8xXloMnMGPu2', '1', NULL, '2022-05-10 19:57:46', '2023-06-26 23:20:30'),
(8, 'prueba3', 'jesusfernandezcha@gmail.com', NULL, '$2y$10$nzNkVThl9D7qWv8VC9diZeN.TxRNTU3C.zZSxC89i232qK3EHmy/m', '2', NULL, '2022-05-10 21:48:58', '2023-06-27 20:13:05');

-- --------------------------------------------------------

--
-- Table structure for table `vacaciones`
--

CREATE TABLE `vacaciones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `total_dias` varchar(255) NOT NULL,
  `feriados` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vacaciones_empleados`
--

CREATE TABLE `vacaciones_empleados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `empleado_id` bigint(20) UNSIGNED NOT NULL,
  `vacaciones_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `correos`
--
ALTER TABLE `correos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `correos_tipo_index` (`tipo`);

--
-- Indexes for table `creditos`
--
ALTER TABLE `creditos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `creditos_empleados`
--
ALTER TABLE `creditos_empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creditos_empleados_credito_id_index` (`credito_id`),
  ADD KEY `creditos_empleados_empleado_id_index` (`empleado_id`);

--
-- Indexes for table `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cuentas_banco_index` (`banco`),
  ADD KEY `cuentas_tipo_index` (`tipo`);

--
-- Indexes for table `direcciones`
--
ALTER TABLE `direcciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `direcciones_estado_index` (`estado`),
  ADD KEY `direcciones_municipio_index` (`municipio`),
  ADD KEY `direcciones_parroquia_index` (`parroquia`);

--
-- Indexes for table `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documentos_tipo_index` (`tipo`);

--
-- Indexes for table `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empleados_persona_index` (`persona`),
  ADD KEY `empleados_cargo_index` (`cargo`),
  ADD KEY `empleados_correo_index` (`correo`),
  ADD KEY `empleados_contacto_index` (`contacto`);

--
-- Indexes for table `empleados_conocimientos`
--
ALTER TABLE `empleados_conocimientos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empleados_conocimientos_empleado_id_index` (`empleado_id`),
  ADD KEY `empleados_conocimientos_conocimiento_id_index` (`conocimiento_id`);

--
-- Indexes for table `empleados_cuentas`
--
ALTER TABLE `empleados_cuentas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empleados_cuentas_empleado_id_index` (`empleado_id`),
  ADD KEY `empleados_cuentas_cuenta_id_index` (`cuenta_id`);

--
-- Indexes for table `empleados_documentos`
--
ALTER TABLE `empleados_documentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `empleados_documentos_empleado_id_index` (`empleado_id`),
  ADD KEY `empleados_documentos_documento_id_index` (`documento_id`);

--
-- Indexes for table `evaluaciones`
--
ALTER TABLE `evaluaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evaluaciones_empleados`
--
ALTER TABLE `evaluaciones_empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `evaluaciones_empleados_empleado_id_index` (`empleado_id`),
  ADD KEY `evaluaciones_empleados_evaluacion_id_index` (`evaluacion_id`);

--
-- Indexes for table `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventos_tipo_index` (`tipo`);

--
-- Indexes for table `eventos_empleados`
--
ALTER TABLE `eventos_empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventos_empleados_empleado_id_index` (`empleado_id`),
  ADD KEY `eventos_empleados_evento_id_index` (`evento_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `nomencladores`
--
ALTER TABLE `nomencladores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `personal_access_tokens_2`
--
ALTER TABLE `personal_access_tokens_2`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personas_nacionalidad_index` (`nacionalidad`),
  ADD KEY `personas_talla_superior_index` (`talla_superior`),
  ADD KEY `personas_talla_inferior_index` (`talla_inferior`),
  ADD KEY `personas_talla_calzado_index` (`talla_calzado`),
  ADD KEY `personas_grupo_sanguineo_index` (`grupo_sanguineo`),
  ADD KEY `personas_telefono_movil_index` (`telefono_movil`),
  ADD KEY `personas_telefono_local_index` (`telefono_local`),
  ADD KEY `direccion_habitacion` (`direccion_habitacion`);

--
-- Indexes for table `personas_enfermedades`
--
ALTER TABLE `personas_enfermedades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personas_enfermedades_persona_id_index` (`persona_id`),
  ADD KEY `personas_enfermedades_enfermedad_id_index` (`enfermedad_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sidebars`
--
ALTER TABLE `sidebars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telefonos`
--
ALTER TABLE `telefonos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `telefonos_tipo_index` (`tipo`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vacaciones`
--
ALTER TABLE `vacaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vacaciones_empleados`
--
ALTER TABLE `vacaciones_empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vacaciones_empleados_empleado_id_index` (`empleado_id`),
  ADD KEY `vacaciones_empleados_vacaciones_id_index` (`vacaciones_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `correos`
--
ALTER TABLE `correos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `creditos`
--
ALTER TABLE `creditos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `creditos_empleados`
--
ALTER TABLE `creditos_empleados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cuentas`
--
ALTER TABLE `cuentas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `direcciones`
--
ALTER TABLE `direcciones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `empleados_conocimientos`
--
ALTER TABLE `empleados_conocimientos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `empleados_cuentas`
--
ALTER TABLE `empleados_cuentas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `empleados_documentos`
--
ALTER TABLE `empleados_documentos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `evaluaciones`
--
ALTER TABLE `evaluaciones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `evaluaciones_empleados`
--
ALTER TABLE `evaluaciones_empleados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eventos_empleados`
--
ALTER TABLE `eventos_empleados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `nomencladores`
--
ALTER TABLE `nomencladores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_access_tokens_2`
--
ALTER TABLE `personal_access_tokens_2`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `personas`
--
ALTER TABLE `personas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `personas_enfermedades`
--
ALTER TABLE `personas_enfermedades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sidebars`
--
ALTER TABLE `sidebars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `telefonos`
--
ALTER TABLE `telefonos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `vacaciones`
--
ALTER TABLE `vacaciones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vacaciones_empleados`
--
ALTER TABLE `vacaciones_empleados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `correos`
--
ALTER TABLE `correos`
  ADD CONSTRAINT `correos_tipo_foreign` FOREIGN KEY (`tipo`) REFERENCES `nomencladores` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `creditos_empleados`
--
ALTER TABLE `creditos_empleados`
  ADD CONSTRAINT `creditos_empleados_credito_id_foreign` FOREIGN KEY (`credito_id`) REFERENCES `creditos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `creditos_empleados_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cuentas`
--
ALTER TABLE `cuentas`
  ADD CONSTRAINT `cuentas_banco_foreign` FOREIGN KEY (`banco`) REFERENCES `nomencladores` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cuentas_tipo_foreign` FOREIGN KEY (`tipo`) REFERENCES `nomencladores` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `direcciones`
--
ALTER TABLE `direcciones`
  ADD CONSTRAINT `direcciones_estado_foreign` FOREIGN KEY (`estado`) REFERENCES `nomencladores` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `direcciones_municipio_foreign` FOREIGN KEY (`municipio`) REFERENCES `nomencladores` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `direcciones_parroquia_foreign` FOREIGN KEY (`parroquia`) REFERENCES `nomencladores` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `documentos`
--
ALTER TABLE `documentos`
  ADD CONSTRAINT `documentos_tipo_foreign` FOREIGN KEY (`tipo`) REFERENCES `nomencladores` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `empleados`
--
ALTER TABLE `empleados`
  ADD CONSTRAINT `empleados_cargo_foreign` FOREIGN KEY (`cargo`) REFERENCES `nomencladores` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `empleados_contacto_foreign` FOREIGN KEY (`contacto`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `empleados_correo_foreign` FOREIGN KEY (`correo`) REFERENCES `correos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `empleados_persona_foreign` FOREIGN KEY (`persona`) REFERENCES `personas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `empleados_conocimientos`
--
ALTER TABLE `empleados_conocimientos`
  ADD CONSTRAINT `empleados_conocimientos_conocimiento_id_foreign` FOREIGN KEY (`conocimiento_id`) REFERENCES `nomencladores` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `empleados_conocimientos_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `empleados_cuentas`
--
ALTER TABLE `empleados_cuentas`
  ADD CONSTRAINT `empleados_cuentas_cuenta_id_foreign` FOREIGN KEY (`cuenta_id`) REFERENCES `cuentas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `empleados_cuentas_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `empleados_documentos`
--
ALTER TABLE `empleados_documentos`
  ADD CONSTRAINT `empleados_documentos_documento_id_foreign` FOREIGN KEY (`documento_id`) REFERENCES `documentos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `empleados_documentos_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `evaluaciones_empleados`
--
ALTER TABLE `evaluaciones_empleados`
  ADD CONSTRAINT `evaluaciones_empleados_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `evaluaciones_empleados_evaluacion_id_foreign` FOREIGN KEY (`evaluacion_id`) REFERENCES `evaluaciones` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `eventos`
--
ALTER TABLE `eventos`
  ADD CONSTRAINT `eventos_tipo_foreign` FOREIGN KEY (`tipo`) REFERENCES `nomencladores` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `eventos_empleados`
--
ALTER TABLE `eventos_empleados`
  ADD CONSTRAINT `eventos_empleados_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleados` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `eventos_empleados_evento_id_foreign` FOREIGN KEY (`evento_id`) REFERENCES `eventos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `personas_enfermedades`
--
ALTER TABLE `personas_enfermedades`
  ADD CONSTRAINT `personas_enfermedades_enfermedad_id_foreign` FOREIGN KEY (`enfermedad_id`) REFERENCES `nomencladores` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `personas_enfermedades_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `personas` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
