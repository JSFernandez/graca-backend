<?php

use Database\Seeders\SidebarSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(RoleSeeder::class);
        // $this->call(DireccionSeeder::class);
        // $this->call(TelefonoSeeder::class);
        // $this->call(emailSeeder::class);
        // $this->call(PersonaSeeder::class);
        // $this->call(EmpleadoSeeder::class);
        // $this->call(UserSeeder::class);
        $this->call(SidebarSeeder::class);
    }
}
