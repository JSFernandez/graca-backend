<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = Role::create(['name'=>'super-admin']);
        $role2 = Role::create(['name'=>'admin']);
        $role3 = Role::create(['name'=>'supervisor']);
        $role4 = Role::create(['name'=>'empleado']);

        Permission::create(['name'=>'read']);
        Permission::create(['name'=>'create']);
        Permission::create(['name'=>'update']);
        Permission::create(['name'=>'delete']);
    }


}
