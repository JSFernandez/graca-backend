<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(\App\Models\User::class, 20)->create();
        DB::table('users')->insert([
            [
                'id'                =>1,
                'name'              => 'admin',
                'email'             => 'admin@gmail.com',
                'password'          => bcrypt('123456'),
                'empleado'          => 1,
                'status'           => 'active',
                'email_verified_at' => now(),
                'created_at'        => now(),
                'updated_at'        => now(),
            ],
        ]);
    }
}
