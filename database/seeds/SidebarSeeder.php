<?php

namespace Database\Seeders;

use App\Models\Sidebar;
use Illuminate\Database\Seeder;

class SidebarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sidebar::create(['path' => '/dashboard','title' =>'Panel principal','icon'=> 'dashboard','class' =>'']);
        Sidebar::create(['path' => '/user-profile','title' =>'Mi perfil','icon'=> 'badge','class' =>'']);
        Sidebar::create(['path' => '/role-permission','title' =>'Roles-Permisos','icon'=> 'enhanced_encryption','class' =>'']);
        Sidebar::create(['path' => '/usuarios','title' =>'Usuarios','icon'=> 'people','class' =>'']);
        Sidebar::create(['path' => '/empleados','title' =>'Empleados','icon'=> 'groups','class' =>'']);
        Sidebar::create(['path' => '/documentos','title' =>'Documentos','icon'=> 'picture_as_pdf','class' =>'']);
        Sidebar::create(['path' => '/geolocalizacion','title' =>'Geolocalización','icon'=> 'groups','class' =>'']);
        Sidebar::create(['path' => '/calendario','title' =>'Calendario','icon'=> 'event','class' =>'']);
    }
}
