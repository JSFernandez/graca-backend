<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\models\Direccion;
use App\Models\Nomenclador;
use Faker\Generator as Faker;

$factory->define(Direccion::class, function (Faker $faker) {
    return [
        'estado'   			=> Nomenclador::all()->where('tipo',107)->random()->id,
        'municipio' 		=> Nomenclador::all()->where('tipo',108)->random()->id,
        'parroquia' 		=> Nomenclador::all()->where('tipo',108)->random()->id,
		'detalles'         	=> $faker->address,
        'codigo_postal'     => '1030',
    ];
});
