<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Persona;
use App\Models\Direccion;
use App\Models\Nomenclador;
use App\Models\Telefono;
use Faker\Generator as Faker;

// $faker = Faker\Factory::create('es_VE'); 

$factory->define(Persona::class, function (Faker $faker) {
    return [
        'cedula'            => $faker->ean8,
        'nacionalidad'      => $faker->randomElement($array = array('V', 'E')),
        'p_nombre'          => $faker->firstName,
        's_nombre'          => $faker->firstName,
        's_apellido'        => $faker->lastName,
        'p_apellido'        => $faker->lastName,
        'fecha_nacimiento'  => $faker->date,
        'sexo'              => $faker->randomElement($array = array('Femenina', 'Masculino')),
        'imagen'            => 'default.png',
        'status'            => $faker->randomElement($array = array('activo', 'inactivo')),
        'talla_camisa'      => Nomenclador::all()->where('tipo', 8)->random()->id,
        'talla_pantalon'    => Nomenclador::all()->where('tipo', 9)->random()->id,
        'talla_calzado'     => Nomenclador::all()->where('tipo', 10)->random()->id,
        'grupo_sanguineo'   => Nomenclador::all()->where('tipo', 3)->random()->valor,
        'telefono_movil'    => Telefono::all()->random()->id,
        'telefono_local'    => Telefono::all()->random()->id,
        'direccion_habitacion' => Direccion::all()->random()->id,
    ];
});
