<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacaciones extends Model
{
    use HasFactory;
    protected $table = 'vacaciones';

    protected $fillable = [
       'id',
       'fecha_inicio',
       'fecha_fin',
       'dias_solicitados',
       'dias_restantes',
       'dias_feriados',
       'periodo',
       'status'
    ];

    /**
     * [Empleados relación de N:N]
     * @return  [type]  [return description]
     */
    public function VacacionesEmpleado()
    {
        return $this->belongsToMany(Empleado::class,'vacaciones_empleados','vacaciones_id','empleado_id')->withTimestamps();
    }
}
