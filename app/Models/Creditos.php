<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Creditos extends Model
{
    use HasFactory;
    protected $table = 'creditos';

    protected $fillable = [
       'id', 'monto', 'moneda', 'cuotas', 'fecha_inicio', 'fecha_fin','monto_descuento','frecuencia_descuento', 'status'
    ];

     /**
     * [empleado description]
     * @return  [type]  [return description]
     */
    public function CreditoEmpleado()
    {
        return $this->belongsToMany(Empleado::class,'creditos_empleados','credito_id','empleado_id')->withTimestamps();
    }

}
