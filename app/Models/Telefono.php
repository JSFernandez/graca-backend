<?php

namespace App\Models;

use App\models\Persona;
use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
    protected $table = 'telefonos';

    protected $fillable = [
        'numero','status','tipo','status'
    ];


    /**
     * [Empleados relación de N:N]
     * @return  [type]  [return description]
     */
    public function Personas()
    {
        return $this->belongsTo(Persona::class,'id');
    }
}
