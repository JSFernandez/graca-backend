<?php

namespace App\Models;

use App\models\Persona;
use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    public $timestamps = false;
    protected $table = 'direcciones';

    protected $fillable = [
       'estado', 'municipio', 'parroquia', 'detalles', 'codigo_postal'
    ];


    /**
     * [empleado description]
     * @return  [type]  [return description]
     */
    public function Persona()
    {
        return $this->belongsTo(Persona::class,'id');
    }
}
