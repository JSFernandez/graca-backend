<?php

namespace App\Models;

use App\models\Persona;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Empleado extends Model
{
    protected $table = 'empleados';

    protected $fillable = [
        'foto',
        'ct',
        'cargo',
        'fecha_ingreso',
        'status',
        'persona',
        'contacto',
        'correo',
        'honorarios'
    ];

    /**
     * [usuario relación de 1:N]
     * @return  [type]  [return description]
     */
    public function usuario()
    {
        return $this->hasMany('App\User');
    }
    /**
     * [persona relación 1:1]
     * @return  [type]  [return description]
     */
    public function Persona():BelongsTo
    {
        return $this->belongsTo(Persona::class,'persona');
    }
    /**
     * [persona relación 1:1]
     * @return  [type]  [return description]
     */
    public function PersonaContacto()
    {
        return $this->belongsTo(Persona::class,'contacto');
    }
    /**
     * [TelefonoLocal description]
     *
     * @return  [type]  [return description]
     */
    public function TelefonoContacto()
    {
        return $this->hasOne(Telefono::class,'contacto','telefono_local');
    }
    /**
     * [empleado description]
     * @return  [type]  [return description]
     */
    public function Correo()
    {
        return $this->belongsTo(Correo::class,'correo');
    }
     /**
     * [empleado description]
     * @return  [type]  [return description]
     */
    public function Cargo()
    {
        return $this->belongsTo(Nomenclador::class,'cargo');
    }

    /**
     * [credito description]
     *
     * @return  [type]  [return description]
     */
    public function CreditoEmpleado()
    {
        return $this->belongsToMany(Creditos::class);
    }

    /**
     * [Vacaciones description]
     *
     * @return  [type]  [return description]
     */
    public function VacacionesEmpleado()
    {
        return $this->belongsToMany(Vacaciones::class,'vacaciones_empleados');
    }


}
