<?php

namespace App\models;

use App\Models\Direccion;
use App\Models\Nomenclador;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Persona extends Model
{
    protected $table = 'personas';

    protected $fillable = [
        'cedula',
        'nacionalidad',
        'p_nombre',
        'p_apellido',
        's_nombre',
        's_apellido',
        'fecha_nacimiento',
        'sexo',
        'grupos_sanguineo',
        'status',
        'foto',
        'talla_superior',
        'talla_inferior',
        'talla_calzado',
        'telefono_movil',
        'telefono_local',
        'telefono_contacto',
        'direccion_habitacion'
    ];

    public function Empleado():HasOne
    {
        return $this->hasOne(Empleado::class,'persona');
    }

    public function Direccion()
    {
        return $this->belongsTo(Direccion::class, 'direccion_habitacion');
    }


    public function Nacionalidad()
    {
        return $this->hasOne(Nomenclador::class,'tipo');
    }

    public function fullName()
    {
        return $this->p_nombre.' '.$this->p_apellido;
    }

    public function TelefonoMovil()
    {
        return $this->belongsTo(Telefono::class,'telefono_movil');
    }

    public function TelefonoLocal()
    {
        return $this->belongsTo(Telefono::class,'telefono_local');
    }

}

