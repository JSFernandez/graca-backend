<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Throwable;

class AuthController extends Controller
{


    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login', 'register']]);
    }

    public function register()
    {
        $user = new User(request()->all());
        $user->password = bcrypt($user->password);
        $user->save();
        return $user;
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['message' => 'Email o calve errada, por favor intente nuevamente', 'code' => 1001]);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        try {
            $stmt = User::find(auth()->user()->id);
            $stmt->assignRole('admin');
            return response()->json([
                'code' => 1000,
                'access_token' => $token,
                'token_type' => 'bearer',
                'data' => [
                    'user' => auth()->user()->name,
                    'role' => $stmt->getRoleNames(),
                    'permissions' => $stmt->getDirectPermissions()
                ],
            ]);
        } catch (Throwable $e) {
            return response()->json([
                'code' => 1000,
                'data' => null,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            auth()->logout();
            return response()->json([
                'code' => 1000,
                'data' => null,
                'message' => 'Successfully logged out'
            ]);
        } catch (Throwable $e) {
            return response()->json([
                'code' => 1000,
                'data' => null,
                'message' => $e->getMessage()
            ]);
        }
    }
}
