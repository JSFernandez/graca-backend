<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use Mail;
use Exception;
use Carbon\Carbon;
use DateTimeZone;
use NumberFormatter;




class DocumentosController extends Controller
{
    public function __construct(){

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->view('pdf.template.template');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


    public function GeneratePDF(Request $data)
    {
        // return response()->json($data->data['nombres']);



        ($messageData = $data->data);
        $messageData = collect($messageData);
        $monto = $this->montoNumLetras($messageData['honorarios']);
        $messageData->put('monto',$monto);

        $date = Carbon::create($messageData['fecha_ingreso'])->locale('es_ES');
        $date = $date->isoFormat('LL');
        $messageData->put('fecha_ingreso',$date);

        $pdf = PDF::loadView('pdf.template.template',compact('messageData'));
        $pdf->setPaper('letter', 'portrait');

        // $pdf = PDF::loadView('emails/templates/invoice-pdf', $data);

        $receivers = 'sherman589@gmail.com';
        // Mail::to($receivers)->send(new EmergencyCallReceived($messageData));
        if ($data->tipo) {
            $pdf->save(storage_path('app/public/') . 'archivo.pdf');
            return response()->json([
                "code"=>1000,
                "url"=>"http://localhost/graca-backend/storage/app/public/archivo.pdf",
                "message"=>"Consulta exitosa"
            ]);
        }else{
            try {
                Mail::send('mails.emergency_call',compact('messageData'), function ($mail) use ($pdf) {
                    $mail->from('jesusfernandezcha@gmail.com', 'John Doe');
                    $mail->to('sherman589@gmail.com');
                    $mail->attachData($pdf->output(), 'constancia.pdf');
                });
                // return $pdf->stream('ejemplo.pdf');
                return response()->json(['code'=>1000, 'message'=>'Documento fue enviado exitosamente']);
            } catch (Exception $e) {
                return response()->json('Error: No se pudo enviar el correo'.$e);
            }
        }
    }

    public function montoNumLetras($monto){
        $f = new NumberFormatter("es", NumberFormatter::SPELLOUT);
        $izquierda = intval(floor($monto));
        $derecha = intval(($monto - floor($monto)) * 100);
        return $monto = $f->format($izquierda) . " bolívares con " . $f->format($derecha). " céntimos";
    }

}
