<?php

namespace App\Http\Controllers;

use App\Http\Requests\Empleado\CreateRequest;
use App\Http\Resources\combosBoxCollection;
use App\Http\Resources\CombosDatosPersonales;
use App\Http\Resources\EmpleadoResource;
use App\Http\Resources\empleadosCollection;
use App\Models\Empleado;
use App\Models\Nomenclador;
use App\Repositories\EmpleadoRerpository;
use GuzzleHttp\Psr7\Message;
use Illuminate\Http\Request;
use Nette\Utils\Strings;
use Throwable;

class EmpleadoController extends Controller
{

    private $stmt;

    public function __construct(
        EmpleadoRerpository $repository,
        Nomenclador $repoNomenclador
        )
    {
        $this->stmt = $repository;
        $this->nomenclado = $repoNomenclador;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return empleadosCollection::make(Empleado::all());
    }

    public function combos(){
        // return combosBoxCollection::make($this->stmt->getCombos());
        return response()->json([
            'data' =>$this->stmt->getCombos(),
            'code' => '1000',
            'message' => 'Datos consultados Correctamente'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        // return response()->json($request->all());
        $data = $request;
        try {
            $empleado = $this->stmt->nuevoRegistro($data);
            return response()->json([
                'type'      =>'Empleado',
                'data'      => $empleado,
                'code'      => '1000',
                'message'   => 'Registro Exitoso'
            ]);
        } catch (Throwable $e) {
            return response()->json([
                'code' => '1001',
                'message' => $e->getMessage()
                // 'message' => $empleado
            ]);
        }
        // Session::flash('message-success',$request->p_nombre.' '.$request->p_apellido.' fue registrado exitosamente');
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Empleado $empleado)
    {
        // dd($user);
        // $this->stmt->get
        // return response()->json($user);
    }

    public function get($id){
        $data = $id;
        return response()->json($data, 200);
      }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
         try {
            $empleado = $this->stmt->_update($request);
            return response()->json([
                'data' => $empleado,
                'message' => 'Registro fué actualizado exitosamente',
                'success' => true
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'code' => '1001',
                'message' => $e->getMessage()
            ]);
        }
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * [search description]
     *
     * @param   Request  $cedula  [$cedula description]
     * @return  [type]            [return description]
     */
    public function search(Request $cedula){
        // return response()->json($cedula);
      return $this->stmt->searchEmpleado($cedula);
    }
}
