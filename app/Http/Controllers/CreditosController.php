<?php

namespace App\Http\Controllers;

use App\Models\Creditos;
use App\Http\Requests\StoreCreditosRequest;
use App\Http\Requests\UpdateCreditosRequest;
use App\Http\Resources\CreditosCollection;
use App\Repositories\CreditoRepository;
use Illuminate\Http\Request;
use Throwable;

class CreditosController extends Controller
{
    private $repository;

    public function __construct(
        CreditoRepository $repo
        )
    {
        $this->repository = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CreditosCollection::make(Creditos::all()->where('status','!=',0));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCreditosRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
        $credito = $this->repository->create($request->all());
        return response()->json(
            [
                'code' => 1000,
                'data' => $credito,
                'message' => 'Consulta exitosaa'
            ]
            );
       } catch (Throwable $th) {
        return response()->json(
            [
                'code' => 1001,
                'message' => $th->getMessage()
            ]
            );
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function show(Request $credito)
    {
        return response()->json(
            [
                'code' => 1000,
                'data' => $credito,
                'message' => 'Consulta exitosa'
            ]

        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function edit(Creditos $creditos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCreditosRequest  $request
     * @param  \App\Models\Creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, String $id)
    {
        return $this->repository->update($request->all(),$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Creditos  $creditos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        return $this->repository->delete($id);
    }
}
