<?php

namespace App\Http\Controllers;

use App\Models\Vacaciones;
use App\Http\Requests\StoreVacacionesRequest;
use App\Http\Requests\UpdateVacacionesRequest;
use App\Http\Resources\vacacionesCollention;
use App\Repositories\VacacionesRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Throwable;

class VacacionesController extends Controller
{
    private $repository;

    public function __construct(
        VacacionesRepository $repo
    ) {
        $this->repository = $repo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return vacacionesCollention::make(Vacaciones::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreVacacionesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->repository->_create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vacaciones  $vacaciones
     * @return \Illuminate\Http\Response
     */
    public function show(Vacaciones $vacaciones)
    {
        //
    }

    /**
     * [update description]
     *
     * @param   Request  $request  [$request description]
     * @param   String   $id       [$id description]
     * @return  [type]             [return description]
     */
    public function update(Request $request, String $id)
    {
        $request = $request->all();
        $request['fecha_inicio'] = Carbon::parse($request['fecha_inicio'])->format('Y-m-d');
        $request['fecha_fin'] = Carbon::parse($request['fecha_fin'])->format('Y-m-d');
        return $this->repository->update($request,$id);

    }

    /**
     * [search description]
     *
     * @param   Request  $request  [$request description]
     * @return  [type]             [return description]
     */
    public function search(Request $request)
    {
        return $this->repository->search($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vacaciones  $vacaciones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vacaciones $vacaciones)
    {
        //
    }
}
