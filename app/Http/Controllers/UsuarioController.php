<?php

namespace App\Http\Controllers;

use App\Http\Resources\usuariosCollection;
use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\UserRerpository;
use Throwable;

class UsuarioController extends Controller
{
    private $repository;

    public function __construct(
        UserRerpository $repo
        )
    {
        $this->repository = $repo;
    }
    /**
     * Undocumented function
     *
     * @return void
     */
    public function index()
    {
        // return User::all();
        return usuariosCollection::make(User::all()->where('status','!=','0'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = $this->repository->createUser($request->all());
            return response()->json([
                'code' => '1000',
                'data' => $user,
                'message' => 'Registro fué actualizado exitosamente',
                'success' => true
            ], 200);
        } catch (Throwable $e) {
            return response()->json([
                'code' => '1001',
                'message' => $e->getMessage()
                // 'message' => $empleado
            ]);
        }
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request, $id){
        try {
           $user = $this->repository->updateUser($request->all());
           return response()->json([
               'code' => '1000',
               'data' => $user,
               'message' => 'Registro fué actualizado exitosamente',
               'success' => true
           ], 200);
       } catch (\Throwable $e) {
           return response()->json([
               'code' => '1001',
               'message' => $e->getMessage()
           ]);
       }
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function destroy(Request $request, $id)
    {
        try {
            $user = $this->repository->findOrFail($id);
            $user->status = '0';
            $user = $this->repository->delete($user);
            return response()->json([
                'code' => '1000',
                'data' => $user,
                'message' => 'Registro fué actualizado exitosamente',
                'success' => true
            ], 200);
        } catch (\Throwable $e) {
            return response()->json([
                'code' => '1001',
                'message' => $e->getMessage()
            ]);
        }
    }
}
