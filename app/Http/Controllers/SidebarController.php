<?php

namespace App\Http\Controllers;

use App\Models\Sidebar;
use App\Http\Requests\StoresidebarRequest;
use App\Http\Requests\UpdatesidebarRequest;
use App\Http\Resources\sidebarCollection;

class SidebarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return sidebarCollection::make(Sidebar::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoresidebarRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoresidebarRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\sidebar  $sidebar
     * @return \Illuminate\Http\Response
     */
    public function show(sidebar $sidebar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\sidebar  $sidebar
     * @return \Illuminate\Http\Response
     */
    public function edit(sidebar $sidebar)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatesidebarRequest  $request
     * @param  \App\Models\sidebar  $sidebar
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatesidebarRequest $request, sidebar $sidebar)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\sidebar  $sidebar
     * @return \Illuminate\Http\Response
     */
    public function destroy(sidebar $sidebar)
    {
        //
    }
}
