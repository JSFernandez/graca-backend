<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class sidebarCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'type'  => 'Sidebar',
            'data' => SidebarResource::collection($this->collection),
            'links'=>[
            ],
            'code' => 1000,
            'message' => 'Consulta exitosa'
        ];
    }
}
