<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class sidebarResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $roles = User::find(Auth::User()->id)->getRoleNames();

        return [
            'path'  => $this->path,
            'title' => $this->title,
            'icon'  => $this->icon,
            'class' => $this->class,
            'roles' => $roles,
        ];
    }
}
