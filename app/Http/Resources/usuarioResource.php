<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

// use Spatie\Permission\Contracts\Role;

class usuarioResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'type'  => 'User',
            'id'    => (string)$this->id,
            'name'  => $this->name,
            'email' => $this->email,
            'status'=> $this->status,
            'role'  => $this->getRoleNames(),
            'permissions' => $this->getPermissionNames()
        ];
    }
}
