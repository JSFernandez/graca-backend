<?php

namespace App\Http\Resources;

use App\Models\Empleado;
use Illuminate\Http\Resources\Json\ResourceCollection;

class empleadosCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => EmpleadoResource::collection($this->collection),
            'links'=>[
            //   'self' => route('/api/v1/empleado')
            ],
            'code' => 1000,
            'message' => 'Consulta exitosa'
        ];
    }
}
