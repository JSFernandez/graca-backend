<?php

namespace App\Http\Resources;

use App\Models\Creditos;
use App\models\Persona;
use Illuminate\Http\Resources\Json\JsonResource;

class CreditosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $persona = Persona::find($this->CreditoEmpleado[0]->persona);
        return [
            'type' => 'Credito',
            "empleado" => $persona->p_nombre.' '.$persona->p_apellido,
            "cedula" => $persona->cedula,
            "id" => $this->id,
            "monto" => $this->monto,
            "fecha_inicio" => $this->fecha_inicio,
            "fecha_fin" => $this->fecha_fin,
            "cuotas" => $this->cuotas,
            "moneda" => $this->moneda,
            "monto_descuento" => $this->monto_descuento,
            "frecuencia_descuento" => $this->frecuencia_descuento,
            "status" => $this->status,
        ];
    }
}
