<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

use function PHPSTORM_META\type;

class EmpleadoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'type' => 'Empleado',
            'id' => $this->id,
            'nombres' => $this->Persona->p_nombre.' '. Str::substr($this->Persona->s_nombre, 0, 1).'.',
            'apellidos' => $this->Persona->p_apellido.' '. Str::substr($this->Persona->s_apellido, 0, 1).'.',
            'sexo'=> $this->Persona->sexo,
            'fecha_nacimiento' => $this->Persona->fecha_nacimiento,
            'ct' => $this->ct,
            'cargo' => $this->Cargo,
            'honorarios' => $this->honorarios,
            'fecha_ingreso' => $this->fecha_ingreso,
            'status' => $this->status,
            'correo' => $this->Correo,
            'persona'=> $this->Persona,
            'telefono_movil' => $this->Persona->TelefonoMovil,
            'telefono_local' => $this->Persona->TelefonoLocal,
            'telefono_contacto'=>$this->Persona->TelefonoLocal,
            'direccion'=> $this->Persona->Direccion,
            'persona_contacto' => $this->PersonaContacto,
        ];
    }
}
