<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CreditosCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => CreditosResource::collection($this->collection),
            'links'=>[
            //   'self' => route('/api/v1/empleado')
            ],
            'code' => 1000,
            'message' => 'Consulta exitosa'
        ];
    }
}
