<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Spatie\Permission\Contracts\Role;
use Spatie\Permission\Models\Role as ModelsRole;

class usuariosCollection extends ResourceCollection
{


    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $roles = ModelsRole::all();

        // return parent::toArray($request);
        return [
            'data' =>
            [
                'usuarios'  => UsuarioResource::collection($this->collection),
                'roles'     => $roles
            ],
            'links'=>[
            //   'self' => route('/api/v1/usuario')
            ],
            'code' => 1000,
            'message' => 'Consulta exitosa'
        ];
    }
}
