<?php

namespace App\Http\Resources;

use App\models\Persona;
use Illuminate\Http\Resources\Json\JsonResource;

class VacacionesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $persona = Persona::find($this->VacacionesEmpleado[0]->persona);
        return [
            'id'                =>$this->id,
            'empleado'          =>$persona->p_nombre.' '. $persona->p_apellido,
            'fecha_inicio'      =>$this->fecha_inicio,
            'fecha_fin'         =>$this->fecha_fin,
            'dias_solicitados'  =>$this->dias_solicitados,
            'dias_restantes'    =>$this->dias_restantes,
            "status"            =>$this->status,
        ];
    }
}
