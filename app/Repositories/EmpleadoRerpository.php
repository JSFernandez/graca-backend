<?php

namespace App\Repositories;

use App\Models\Empleado;
use App\Models\Telefono;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class EmpleadoRerpository extends BaseRerpository implements IRerpository
{
    private $stmt;
    private $persona;
    private $direccion;
    private $correo;
    private $nomenclador;
    private $telefono;

    public function __construct()
    {
        $this->stmt = new Empleado();
        $this->nomenclador = new NomencladorRerpository();
        $this->persona = new PersonaRerpository();
        $this->direccion = new DireccionRerpository();
        $this->correo = new CorreoRerpository();
        $this->telefono = new TelefonoRerpository();
    }

    public function getModel()
    {
        return $this->stmt;
    }

    public function getAll()
    {
        $empleados = collect([]);
        $empleados = $this->getModel()->all();
        return $empleados;
    }

    public function getCombos()
    {
        $grupoSanguineo = $this->Collections($this->nomenclador->getAll()->where('tipo', 90)->where('status', 1));
        $tallaCamisa    = $this->Collections($this->nomenclador->getAll()->where('tipo', 96)->where('status', 1));
        $tallaPantalon  = $this->Collections($this->nomenclador->getAll()->where('tipo', 97)->where('status', 1));
        $tallaCalzado   = $this->Collections($this->nomenclador->getAll()->where('tipo', 98)->where('status', 1));
        $estados        = $this->Collections($this->nomenclador->getAll()->where('tipo', 107)->where('status', 1));
        $municipios     = $this->Collections($this->nomenclador->getAll()->where('tipo', 108)->where('status', 1));
        $parroquias     = $this->Collections($this->nomenclador->getAll()->where('tipo', 109)->where('status', 1));
        $cargos         = $this->Collections($this->nomenclador->getAll()->where('tipo', 91)->where('status', 1));

        return $array =
            [
                'grupoSanguineo'    => $grupoSanguineo->push('', 1),
                'tallaCamisa'       => $tallaCamisa->push('', 1),
                'tallaPantalon'     => $tallaPantalon->push('', 1),
                'tallaCalzado'      => $tallaCalzado->push('', 1),
                'estados'           => $estados->push('', 1),
                'municipios'        => $municipios->push('', 1),
                'parroquias'        => $parroquias->push('', 1),
                'cargos'            => $cargos->push('', 1)
            ];
    }

    /**
     * [Collenciones description]
     *
     * @param   Collection  $arreglo  [$arreglo description]
     * @return  [type]                [return description]
     */
    public function Collections(Collection $arreglo)
    {
        $collection = collect();
        foreach ($arreglo as $item) {
            $collection->push(['id' => $item->id, 'value' => $item->valor, 'codigo' => $item->codigo]);
        }
        return $collection;
    }

    /**
     * [nuevoRegistro description]
     * @param   [type]  $data  [$data description]
     * @return  [type]         [return description]
     */
    public function nuevoRegistro($data)
    {
        $dataPersona = $data->all()['datosPersonales'];
        $dataEmpleado = $data->all()['datosLaborales'];

        $empleado = $this->stmt->where('ct', $dataEmpleado['ct'])->first();
        if (isset($empleado)) {
            throw new Exception("El CT N°: " . $dataEmpleado['ct'] . " ya se esnceuntra registrado");
        } else {
            $persona = $this->persona->where('cedula', $dataPersona['cedula'])->first();
            if (isset($persona)) {
                throw new Exception("La C.I. N°: " . $dataPersona['cedula'] . " ya se esnceuntra registrada");
            } else {

                // PASO 1: registros números telefónicos
                if (isset($dataPersona['telefono_movil'])) {
                    $telf_movil = $this->telefono->create(
                        [
                            'numero'    => $dataPersona['telefono_movil'],
                            'tipo'     => 1,
                            'status'    => 1
                        ]
                    );
                    $dataPersona['telefono_movil'] = $telf_movil->id;
                } else {
                    //Si no existe numero telefonico se con valor 1 (indefinido)
                    $dataPersona['telefono_movil'] = 1;
                }

                if (isset($dataPersona['telefono_local'])) {
                    $telf_local = $this->telefono->create(
                        [
                            'numero'    => $dataPersona['telefono_local'],
                            'tipo'     => 1,
                            'status'    => 1
                        ]
                    );
                    $dataPersona['telefono_local'] = $telf_local->id;
                } else {
                    //Si no existe numero telefonico se con valor 1 (indefinido)
                    $dataPersona['telefono_local'] = 1;
                }


                //PASO 2: Registro de persona
                $dataPersona = Arr::add($dataPersona, 'nacionalidad', 1);
                $dataPersona = Arr::add($dataPersona, 'status', 1);

                $persona = $this->persona->create($dataPersona);
                $dataEmpleado = Arr::add($dataEmpleado, 'persona', $persona->id);
                // return $dataEmpleado;

                //PASO 3: Registro nuemro de contacto
                $datosContacto = $data->all()['datosContacto'];
                if (isset($datosContacto['telefono_movil'])) {
                    $telefonoContacto = [
                        "tipo" => 1,
                        'numero' => $datosContacto['telefono_movil'],
                        "status" => 1
                    ];
                    $telefono = $this->telefono->create($telefonoContacto);
                    $telefono = $telefono->id;
                } else {
                    //Si no exitse el numero de telefono del contacto sra = 1 (indefinido)
                    $telefono = 1;
                }

                //PASO: 4 Registro de persona contacto
                $datosContacto = $data->all()['datosContacto'];
                if (isset($datosPersonales['p_nombre'])) {
                    $contacto = [
                        'p_nombre'          => $datosContacto['p_nombre'],
                        'p_apellido'        => $datosContacto['p_apellido'],
                        'nacionalidad'      => 1,
                        'sexo'              => 1,
                        'cedula'            => $datosPersonales['cedula'] . '-' . $dataEmpleado['ct'],
                        'talla_superior'    => 1,
                        'talla_inferior'    => 1,
                        'talla_calzado'     => 1,
                        'telefono_movil'    => $telefono,
                        'telefono_local'    => 1,
                        'correo'            => 1,
                        'status'            => 1
                    ];
                    $personaContacto = $this->persona->create($contacto);
                    $dataEmpleado = Arr::add($dataEmpleado, 'contacto', $personaContacto->id);
                } else {
                    //en caso de que no existan datos de la persona contacto será = 1 (indefinido)
                    $dataEmpleado = Arr::add($dataEmpleado, 'contacto', 1);
                }
                // return $dataEmpleado;

                //PASO 4: Crea el registro de la dirección del empleado
                $direccionVivienda = $data->all()['direccionVivienda'];

                if ($direccionVivienda['estado']) {
                    $direccionVivienda = Arr::add($direccionVivienda, 'codigo_postal', '1030');
                    $direccion = $this->direccion->create($direccionVivienda);
                    $dataEmpleado = Arr::add($dataEmpleado, 'direccion', $direccion->id);
                } else {
                    $dataEmpleado = Arr::add($dataEmpleado, 'direccion', 1);
                }
                // return $dataEmpleado;

                //PASO 5: Crea correo del empleado
                if (isset($dataEmpleado['correo'])) {
                    $correo = $dataEmpleado['correo'];
                    $correo = $this->correo->create(
                        [
                            'direccion' => $correo,
                            'tipo'      => 1,
                            'status'    => 1
                        ]
                    );
                    $dataEmpleado['correo'] = $correo->id;
                } else {
                    $dataEmpleado['correo'] = 1;
                }
                //Valida si tiene fecha de ingreo
                if (isset($dataEmpleado['fecha_ingreso'])) {
                    $dataEmpleado['fecha_ingreso'] = $dataEmpleado['fecha_ingreso'];
                } else {
                    $dataEmpleado['fecha_ingreso'] = date('Y-m-d');
                }
                // return $dataEmpleado;

                //PASO 6: Crea registro del empleado
                $dataEmpleado = Arr::add($dataEmpleado, 'status', 1);
                // return $dataEmpleado;
                return $this->stmt->create($dataEmpleado);
            }
        }
    }

    public function edit(Empleado $empleado)
    {
        return response()->json(
            []
        );
        $dataEmpleado = $this->stmt->find($empleado->id);
    }

    /**
     * [update description]
     * @param Empleado $empleado [$empleado description]
     */
    public function _update($data){
        $empleado = $this->stmt->find($data['id']);
        $datosPersonales = [
            'cedula' => $data->_persona['cedula'],
            'p_nombre'=> $data->_persona['p_nombre'],
            'p_apellido'=> $data->_persona['p_apellido'],
            's_nombre' => $data->_persona['s_nombre'],
            's_apellido' => $data->_persona['s_apellido'],
            'fecha_nacimiento' => $data->_persona['fecha_nacimiento'],
            'sexo' => $data->_persona['sexo'],
            'grupo_sanguineo' => $data->_persona['grupo_sanguineo'],
            'talla_superior' => $data->_persona['talla_superior'],
            'talla_inferior' => $data->_persona['talla_inferior'],
            'talla_calzado' => $data->_persona['talla_calzado'],
            '_telefono_movil' => $data->_persona['telefono_movil'],
            '_telefono_local' => $data->_persona['telefono_local'],
        ];
        $direccion =
        [
            'estado'=> $data->_direccion['estado'],
            'municipio'=> $data->_direccion['municipio'],
            'parroquia'=> $data->_direccion['parroquia'],
            'detalles'=> $data->_direccion['detalles']
        ];
            $contacto = [
            'p_apellido' => $data->datosContacto['p_apellido'],
            'p_nombre' => $data->datosContacto['p_nombre'],
            '_telefono_movil' => $data->datosContacto['telefono_movil'],
        ];
        try {
            $empleado->update([
                'cargo'         => $data['cargo'],
                'ct'            => $data['ct'],
                'fecha_ingreso' => $data['fecha_ingreso'],
                'honorarios'    => $data['honorarios']
            ]);
            $empleado->Correo->update(['direccion'=>$data['correo']]);
            $empleado->Persona->update($datosPersonales);
            $empleado->Persona->TelefonoMovil->update([$datosPersonales['_telefono_movil']]);
            $empleado->Persona->TelefonoLocal->update([$datosPersonales['_telefono_local']]);
            $empleado->Persona->Direccion->update($direccion);
            $empleado->PersonaContacto->update($contacto);
            $empleado->PersonaContacto->TelefonoMovil->update([$contacto['_telefono_movil']]);
            return $empleado;
        } catch (\Throwable $e) {
            return response()->json(
                [
                    'code'=>'1001',
                    'message'=> $e->getMessage(),
                ]
            );
        }
        return response()->json('pase');
    }

    /**
     * [searchEmpleado description]
     *
     * @param   [type]  $ct  [$ct description]
     * @return  [type]       [return description]
     */
    public function searchEmpleado(Request $cedula) {
        $cedula = $cedula->all();
        try {
            $persona = $this->persona->where('cedula',$cedula)->first();
            $empleado = $this->stmt->where('persona',$persona->id)->first();
            return response()->json([
                'code' => 1000,
                'data' => [
                    'empleado'  =>$empleado,
                    'persona'   =>$persona
                ],
                'message' => 'Consulta exitosa'
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'code' => 1001,
                'data' => $cedula,
                'message' => 'No se encontro registro de empleado '
            ],200);
        }
    }
}
