<?php

namespace App\Repositories;

use App\Models\Persona;
use Exception;

class PersonaRerpository extends BaseRerpository implements IRerpository
{
    private $stmt;

    public function __construct()
    {
        $this->stmt = new Persona();
    }

    public function getmodel()
    {
        return $this->stmt;
    }

    public function create($data)
    {
        $user = $this->stmt->where('cedula',$data['cedula'])->first();
        if(isset($user->id)){
            throw new Exception("El usuario con C.I. N°: ".$data['cedula']." Ya se encuenta registrado");
        }else{
            return $this->stmt->create($data);
        }
    }

    public function where($column, $var)
    {
        return $this->stmt->where($column, $var);
    }
}
