<?php

namespace App\Repositories;

use App\Repositories\BaseRerpository;
use App\Repositories\IRerpository;
use App\Models\Creditos;
use App\Models\Empleado;
use App\models\Persona;
use Throwable;

class CreditoRepository extends BaseRerpository implements IRerpository
{
    private $stmt;
    private $persona;
    private $empleado;

    public function __construct(
        Creditos $credito,
        Persona $persona,
        Empleado $empleado
    ) {
        $this->stmt = $credito;
        $this->persona = $persona;
        $this->empleado = $empleado;
    }

    public function getModel()
    {
        return $this->stmt;
    }

    /**
     * [create description]
     *
     * @param   [type]  $data  [$data description]
     * @return  [type]         [return description]
     */
    public function create($data)
    {
        try {
            $persona = $this->persona->where('cedula', $data['cedula'])->first();
            $empleado = $this->empleado->where('persona', $persona->id)->first();
            $credito = $this->stmt->create($data);
            $credito->CreditoEmpleado()->attach($empleado->id);
            return response()->json(
                [
                    'code' => 1000,
                    'data' => $credito,
                    'message' => 'Consulta exitosaa'
                ]
            );
        } catch (Throwable $th) {
            return response()->json(
                [
                    'code' => 1001,
                    'message' => $th->getMessage()
                ]
            );
        }
    }

    /**
     * [updateCredito description]
     *
     * @param   [type]  $request  [$request description]
     * @param   [type]  $id       [$id description]
     * @return  [type]            [return description]
     */
    public function updateCredito($request, $id)
    {
        $object = $this->stmt::FindOrFail($id);
        return $object->update($request);
    }

    /**
     * [delete description]
     *
     * @param   [type]  $id  [$id description]
     * @return  [type]       [return description]
     */
    public function delete($id)
    {
        try {
            $object = $this->stmt::FindOrFail($id);
            $object->status = 0;
            $object->save();
            return response()->JSON(
                [
                    'code' => '1000',
                    'message' => 'Crédito eliminado exitosamente',
                    'data'  => $object
                ]
            );
        } catch (Throwable $e) {
            return response()->json(
                [
                    'code'=>'1001',
                    'message'=> $e->getMessage(),
                ]
            );
        }
    }
}
