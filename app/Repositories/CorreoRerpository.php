<?php

namespace App\Repositories;

use App\Models\Correo;
use Illuminate\Support\Facades\DB;

class CorreoRerpository extends BaseRerpository implements IRerpository
{
    private $stmt;

    public function __construct()
    {
        $this->stmt = new Correo();
    }

    public function getModel()
    {
        return $this->stmt;
    }
    /**
     * [registrar description]
     * @param   Array  $correos  [$correos arreglo de correo a registrar]
     * @return  [type]           [return description]
     */
    public function registrar(Array $correos)
    {
        foreach ($correos as $correo) {
			$this->getModel()->create($correo);
		}
		return $this;
    }

    /**
     * [where description]
     *
     * @param   [type]  $columna   [$columna description]
     * @param   [type]  $operador  [$operador description]
     * @param   [type]  $valor     [$valor description]
     *
     * @return  [type]             [return description]
     */
    public function buscar(string $columna, string $operador, $valor){
        try {
            $correo = DB::table('correos')->where($columna,$operador,$valor)->first();
        } catch (\Throwable $e) {
            response()->json(
                [
                    [
                        'code'=>'1001',
                        'message'=> $e->getMessage(),
                    ]
                ]
            );
        }
    }

    /**
     * [actualizar description]
     *
     * @param   [type]  $request  [$request description]
     * @param   [type]  $id       [$id description]
     *
     * @return  [type]            [return description]
     */
    public function actualizar($request, $id){
        try {
            $correo = $this->stmt->findOrFail($id);
            $correo->update($request);
        } catch (\Throwable $e) {
            response()->json(
                [
                    [
                        'code'=>'1001',
                        'message'=> $e->getMessage(),
                    ]
                ]
            );
        }
    }
}
