<?php

namespace App\Repositories;

use App\Models\Nomenclador;
use App\Models\Sidebar;

class SidebarRepository extends BaseRerpository implements IRerpository
{
    private $stmt;

    public function __construct()
    {
        $this->stmt = new Sidebar();
    }

    /**
     * [getModel description]
     * @return  [type]  [return description]
     */
    public function getModel()
    {
        return $this->stmt;
    }
    public function getAll()
    {
        $modules = collect([]);
        $modules = $this->getModel()->all();
        return $modules;
    }
}
