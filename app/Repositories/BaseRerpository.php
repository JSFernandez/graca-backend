<?php

namespace App\Repositories;

use PhpParser\Node\Expr\Cast\Object_;

abstract class BaseRerpository
{
    /**
	 * [getModel description]
	 * @return [type] [description]
	 */
	abstract function getModel();

	/**
	 * [getfind description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function findOrFail($id)
	{
		return $this->getModel()->find($id);
	}

	/**
	 * [getAll description]
	 * @return [type] [description]
	 */
	public function getAll()
	{
		return $this->getModel()->all();
	}

	/**
	 * [create description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function create($data)
	{
		return $this->getModel()->create($data);
	}

	/**
	 * [update description]
	 * @param  [type] $object [description]
	 * @param  [type] $data   [description]
	 * @return [type]         [description]
	 */
	public function update($data, $id)
	{
		try {
            $object = $this->getModel()->find($id);
			$object->update($data);
			$object->save();
            return response()->json(
                [
                    'data'      =>$object,
                    'code'      =>'1000',
                    'message'   => 'Actualización exitosa'
                ]
                );
		} catch (\Throwable $e) {
			return response()->json(
                [
                    'code'=>'1001',
                    'message'=> $e->getMessage(),
                ]
            );
		}
	}

	/**
	 * [delete description]
	 * @param  [type] $object [description]
	 * @return [type]         [description]
	 */
	public function delete($object)
	{
		try {
			$object->update([$object]);
			return $object->save();
		} catch (\Throwable $e) {
			return response()->json(
                [
                    'code'=>'1001',
                    'message'=> $e->getMessage(),
                ]
            );
		}
	}

	/**
	 * [pluck description]
	 *
	 * @param   [type]  $object  [$object description]
	 * @return  [type]           [return description]
	 */
	public function pluck()
	{
		return $this->getModel()->pluck();
	}

    /**
     * [responseJson description]
     *
     * @param   string  $code     [$code description]
     * @param   object  $data     [$data description]
     * @param   string  $message  [$message description]
     *
     * @return  [type]            [return description]
     */
    public function responseJson(String $code, Object $data = null, String $message){
        return response()->json([
            'code' => $code,
            'data' => $data,
            'message' => $message,
            'success' => true
        ],200);
    }
}
