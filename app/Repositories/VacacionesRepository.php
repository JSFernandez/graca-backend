<?php

namespace App\Repositories;

use App\Models\Empleado;
use App\models\Persona;
use App\Models\Vacaciones;
use Carbon\Carbon;


class VacacionesRepository extends BaseRerpository implements IRerpository
{
    private $stmt;

    public function __construct(Vacaciones $Model)
    {
        $this->stmt = $Model;
    }

    /**
     * [getModel Intacia el modelo para el repositorio]
     * @return  [type]  [return description]
     */
    public function getModel()
    {
        return $this->stmt;
    }

    /**
     * [search description]
     *
     * @param   string  $data  [$data description]
     * @return  [type]         [return description]
     */
    public function search(Array $data){
        try {
            $persona = Persona::where('cedula',$data['cedula'])->first();
            if ($persona) {
                $vacaciones = $persona->Empleado->VacacionesEmpleado->last();
                if ($vacaciones) {
                    if($vacaciones['dias_restantes'] > 1){
                        $dias = ' días';
                    }else{
                        $dias = ' día';
                    }
                    return $this->responseJson('1000',$vacaciones,' '.$persona->p_nombre.' actualmente dispone de '.$vacaciones['dias_restantes'].$dias.' de descanzo.');
                }else{
                    return $this->responseJson('1000',null,' '.$persona->p_nombre.' actualmente dispone de 10 días de descanzo.');
                }
            }else{
                return $this->responseJson('1001',null,'El número de cédula '.$data['cedula'].' no esta asociado a ningun registro de empleado.');
            }
        } catch (\Throwable $e) {
            return $this->responseJson('1001',null, $e->getMessage());
        }
    }
    /**
     * [createUser description]
     *
     * @param   Array       $data  [$data description]
     * @return  Object         [return description]
     */
    function _create(Array $data): Object {
        $data['fecha_inicio'] = Carbon::parse($data['fecha_inicio'])->format('Y-m-d');
        $data['fecha_fin'] = Carbon::parse($data['fecha_fin'])->format('Y-m-d');
        try {
            $persona = Persona::where('cedula',$data['cedula'])->first();
            if ($persona) {
                $empleado = Empleado::where('persona',$persona->id)->first();
                $object = $this->stmt->create($data);
                $object->VacacionesEmpleado()->attach($empleado->id);
                return $this->responseJson('1000',$object,'Registro fué registrado exitosamente');
            }else{
                return $this->responseJson('1001',null,'El número de cédula '.$data['cedula'].' no esta asociado a ningun registro de empleado.');
            }
        } catch (\Throwable $e) {
            return $this->responseJson('1001',null,$e->getMessage());
        }
    }
}
