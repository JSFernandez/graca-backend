<?php

namespace App\Repositories;

use App\Models\User as ModelsUser;
use PhpParser\Node\Stmt;

class UserRerpository extends BaseRerpository implements IRerpository
{
    private $stmt;

    public function __construct(ModelsUser $modelUser)
    {
        $this->stmt = $modelUser;
    }

    /**
     * [getModel Intacia el modelo para el repositorio]
     * @return  [type]  [return description]
     */
    public function getModel()
    {
        return $this->stmt;
    }
    /**
     * [createUser description]
     *
     * @param   Array       $data  [$data description]
     * @return  ModelsUser         [return description]
     */
    function createUser(Array $data): ModelsUser {
        $data['password'] = bcrypt($data['password']);
        $user = $this->stmt->create($data);
        $user->assignRole($data['rol']);
        return $user;
    }

    /**
     * Undocumented function
     *
     * @param Array $data
     * @return ModelsUser
     */
    function updateUser(Array $data) {
        $user = $this->stmt::FindOrFail($data['id']);
        // $    ->assignRole($data['rol']);
        $user->syncRoles($data['rol']);
        return $user = $user->update($data);
    }
}
